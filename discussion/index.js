// CRUD Operations

// Insert one document
/*
    Syntax:
        db.collectionName.insertOne({objects});
*/

db.users.insertOne({
        firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87645321",
            email: "janedoe@mail.com"
        },
        courses: ["CSS", "Javascript", "Pyton"],
        department: "none"
});

// Insert Many (Create)
/*
    Syntax:
        db.collectionName.insertMany([{objectA}, {objectB}]);
*/

db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87645321",
            email: "stephenhawking@mail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87645321",
            email: "neilarmstrong@mail.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
]);

// Finding documents (Read)
/*
    Syntax:
        db.collectionName.find();
        db.collectionName.find({ field: value })
*/

db.users.find();

db.users.find({ firstName: "Stephen" });

// Finding documents with multiple parameters
db.users.find({ lastName: "Armstrong", age: 82 });


// Updating documents

// Updating a single document
/*
    Syntax: 
        db.collectionName.updateOne( {criteria} , {$set: {field: value}} );
*/

db.users.updateOne(
    { firstName: "Test" },
    {
        $set: {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "12345678",
                email: "bill@mail.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations",
            status: "active"
        }
    }
);

// Updating Multiple documents
/*
    db.collectionName.updateMany( {criteria}, {$set: {field: value}} );
*/

db.users.updateMany(
    { department: "none" },
    {
       $set: { department: "HR" }
    }
);

// Deleting documents (Delelte)

// Deleting one document
/*
    Syntax: 
        db.collectionName.deleteOne({criteria});
*/

db.users.deleteOne({
        firstName: "test"
    })

// Deleting multiple document
/*
    Syntax: 
        db.collectionName.deleteMany({criteria});
*/

db.users.deleteMany({
    firstName: "Bill"
});